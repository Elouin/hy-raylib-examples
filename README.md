# Hy Raylib Examples

These are (some) of the examples for [raylib](https://www.raylib.com/)
implemented in [hy](https://docs.hylang.org/en/stable/) using the [bindings
for python](https://pypi.org/project/raylib/).

## How to

Simply do `hy <filename>.hy` and enjoy. If you don't have hy installed,
simply create a new venv(`python3 -m venv venv`), activate it (`source venv/bin/activate`)
and then install everythingfrom the requirements txt(`pip install -r requirements.txt`).

Quickstart example:
```lisp
(import pyray *)

(init-window 800 450 "Hello")

(while (not (window-should-close))
  (begin-drawing)
  (clear-background WHITE)
  (draw-text "Hello World" 190 200 20 VIOLET)
  (end-drawing))

(close-window)
```

## Disclaimer

I am no lisp expert, so this is propably not the most idiomatic lisp code. If you have
suggestions on how to make them more idiomatic, i am open for suggestions.