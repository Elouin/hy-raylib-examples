(import pyray *)

(setv max-touch-points 10)

(setv screen-width 800)
(setv screen-height 450)

(init-window screen-width screen-height "raylib [core] example - input multitouch")

(setv touch-positions (lfor x (range max-touch-points) (Vector2 0)))

(set-target-fps 60)

(while (not (window-should-close))
  (for [i (range max-touch-points)]
    (setv (get touch-positions i) (get-touch-position i)))
    
  (begin-drawing)
  (clear-background RAYWHITE)
  (for [i (range max-touch-points)]
    (when (and (> (. (get touch-positions i) x) 0) (> (. (get touch-positions i) y) 0)
      (do
        (draw-circle-v (get touch-positions i) 34 ORANGE)
        (draw-text f"{i}" (int (- (. (get touch-positions i) x) 10 )) (int (- (. (get touch-positions i) y) 70)) 40 BLACK)))))
  (draw-text "touch the screen at multiple locations to get multiple balls" 10 10 20 DARKGRAY)
  (end-drawing))
  
(close-window)