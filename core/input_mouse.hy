(import pyray *)

(setv screen-width 800)
(setv screen-height 450)

(init-window screen-width screen-height "raylib [core] example - mouse input")

(setv ball-position (Vector2 -100 -100))
(setv ball-color DARKBLUE)

(set-target-fps 60)

(while (not (window-should-close))
  (setv ball-position (get-mouse-position))
  (cond
    (is-mouse-button-pressed MOUSE-BUTTON-LEFT)
      (setv ball-color MAROON)
    (is-mouse-button-pressed MOUSE-BUTTON-MIDDLE)
      (setv ball-color LIME)
    (is-mouse-button-pressed MOUSE-BUTTON-RIGHT)
      (setv ball-color DARKBLUE)
    (is-mouse-button-pressed MOUSE-BUTTON-SIDE)
      (setv ball-color PURPLE)
    (is-mouse-button-pressed MOUSE-BUTTON-EXTRA)
      (setv ball-color YELLOW)
    (is-mouse-button-pressed MOUSE-BUTTON-FORWARD)
      (setv ball-color ORANGE)
    (is-mouse-button-pressed MOUSE-BUTTON-BACK)
      (setv ball-color BEIGE))
      
    (begin-drawing)
    (clear-background RAYWHITE)
    (draw-circle-v ball-position 40 ball-color)
    (draw-text "move ball with mouse and click mouse button to change color" 10 10 20 DARKGRAY)
    (end-drawing))
    
(close-window)