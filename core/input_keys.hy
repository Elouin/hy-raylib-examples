(import pyray *)

(setv screen-width 800)
(setv screen-height 450)

(init-window screen-width screen-height "raylib [core] example - keyboard input")

(setv ball-position (Vector2 (/ screen-width 2) (/ screen-height 2)))

(set-target-fps 60)

(while (not (window-should-close))
  (when (is-key-down KEY-RIGHT)
    (setv (. ball-position x) (+ (. ball-position x) 2)))
  (when (is-key-down KEY-LEFT)
    (setv (. ball-position x) (- (. ball-position x) 2)))
  (when (is-key-down KEY-UP)
    (setv (. ball-position y) (- (. ball-position y) 2)))
  (when (is-key-down KEY-DOWN)
    (setv (. ball-position y) (+ (. ball-position y) 2)))
    
  (begin-drawing)
  (clear-background RAYWHITE)
  (draw-text "move the ball with arrow keys" 10 10 20 DARKGRAY)
  (draw-circle-v ball-position 50 MAROON)
  (end-drawing))
  
(close-window)