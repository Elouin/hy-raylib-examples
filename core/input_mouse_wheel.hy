(import pyray *)

(setv screen-width 800)
(setv screen-height 450)

(init-window screen-width screen-height "raylib [core] example - input mouse wheel")

(setv box-position-y (- (// screen-height 2) 40))
(setv scroll-speed 4)

(set-target-fps 60)

(while (not (window-should-close))
  (setv box-position-y (- box-position-y (int (* (get-mouse-wheel-move) scroll-speed))))
  
  (begin-drawing)
  (clear-background RAYWHITE)
  (draw-rectangle (- (// screen-width 2) 40) box-position-y 80 80 MAROON)
  (draw-text "Use mouse wheel to move the cube up and down!" 10 10 20 GRAY)
  (draw-text f"Box position Y: {box-position-y}" 10 40 20 LIGHTGRAY)
  (end-drawing))
  
(close-window)