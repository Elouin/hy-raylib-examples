(import pyray *)

(setv max-columns 20)

(setv screen-width 800)
(setv screen-height 450)

(init-window screen-width screen-height "raylib [core] example - 3d camera first person")

(setv camera (Camera3D))
(setv (. camera position) (Vector3 0.0 2.0 4.0))
(setv (. camera target) (Vector3 0.0 2.0 0.0))
(setv (. camera up) (Vector3 0.0 1.0 0.0))
(setv (. camera fovy) 60.0)
(setv (. camera projection) CAMERA-PERSPECTIVE)

(setv camera-mode CAMERA-FIRST-PERSON)

(setv heights [])
(setv positions [])
(setv colors [])

(for [x (range max-columns)]
  (heights.append (get-random-value 1 12))
  (let [x (get-random-value -15 15) y (/ (get heights -1) 2.0) z (get-random-value -15 15)]
    (positions.append (Vector3 x y z)))
  (let [r (get-random-value 20 255) g (get-random-value 10 55) b 30 a 255]
    (colors.append (Color r g b a))))

(disable-cursor)

(set-target-fps 60)

(while (not (window-should-close))
  (cond
    (is-key-pressed KEY-ONE)
      (do
          (setv camera-mode CAMERA-FREE)
          (setv camera.up (Vector3 0.0 1.0 0.0)))
    (is-key-pressed KEY-TWO)
      (do
          (setv camera-mode CAMERA-FIRST-PERSON)
          (setv camera.up (Vector3 0.0 1.0 0.0)))
    (is-key-pressed KEY-THREE)
      (do
          (setv camera-mode CAMERA-THIRD-PERSON)
          (setv camera.up (Vector3 0.0 1.0 0.0)))
    (is-key-pressed KEY-FOUR)
      (do
          (setv camera-mode CAMERA-ORBITAL)
          (setv camera.up (Vector3 0.0 1.0 0.0)))
    (is-key-pressed KEY-P)
      (cond
            (= camera.projection CAMERA-PERSPECTIVE)
              (do
                  (setv camera-mode CAMERA-THIRD-PERSON)
                  (setv camera.position (Vector3 0.0 2.0 -100.0))
                  (setv camera.target (Vector3 0.0 2.0 0.0))
                  (setv camera.up (Vector3 0.0 1.0 0.0))
                  (setv camera.projection CAMERA-ORTHOGRAPHIC)
                  (setv camera.fovy 20.0))
                  ; (camera-yaw camera (* -135 DEG2RAD) True)
                  ; (camera-pitch camera (* -45 DEG2RAD) True True False))
            (= camera.projection CAMERA-ORTHOGRAPHIC)
              (do
                  (setv camera-mode CAMERA-THIRD-PERSON)
                  (setv camera.position (Vector3 0.0 2.0 0.0))
                  (setv camera.target (Vector3 0.0 2.0 0.0))
                  (setv camera.up (Vector3 0.0 1.0 0.0))
                  (setv camera.projection CAMERA-PERSPECTIVE)
                  (setv camera.fovy 60.0))))

  (update-camera camera camera-mode)

  (begin-drawing)
  (clear-background RAYWHITE)
  (begin-mode-3d camera)

  (draw-plane (Vector3 0.0 0.0 0.0) (Vector2 32.0 32.0) LIGHTGRAY)
  (draw-cube (Vector3 -16.0 2.5 0.0) 1.0 5.0 32.0 BLUE)
  (draw-cube (Vector3 16.0 2.5 0.0) 1.0 5.0 32.0 LIME)
  (draw-cube (Vector3 0.0 2.5 16.0) 1.0 5.0 32.0 BLUE)

  (for [i (range max-columns)]
    (draw-cube (get positions i) 2.0 (get heights i) 2.0 (get colors i))
    (draw-cube-wires (get positions i) 2.0 (get heights i) 2.0 MAROON))

  (when (= camera-mode CAMERA-THIRD-PERSON)
    (draw-cube camera.target 0.5 0.5 0.5 PURPLE)
    (draw-cube-wires camera.target 0.5 0.5 0.5 DARKPURPLE))

  (end-mode-3d)
  (end-drawing))

(close-window)
