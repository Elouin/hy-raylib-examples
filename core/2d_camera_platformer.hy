(import pyray *)

(setv G 400)
(setv PLAYER_JUMP_SPD 350)
(setv PLAYER_HOR_SPD 200)

(defclass Player []
  (defn __init__ [self position speed can_jump] 
    (setv self.position position)
    (setv self.speed speed)
    (setv self.can_jump can_jump)))

(defclass EnvItem []
  (defn __init__ [self rect blocking color]
    (setv self.rect rect)
    (setv self.blocking blocking)
    (setv self.color color)))

(defn update_player [player env_items env_items_length delta]
  (cond
    (is_key_down KEY_LEFT)
      (setv player.x (- player.x (* PLAYER_HOR_SPEED delta)))
    (is_key_down KEY_RIGHT)
      (setv player.x (- player.x (* PLAYER_HOR_SPEED delta)))
    (and (is_key_down KEY_SPACE) player.can_jump)
      (do
        (setv player.speed PLAYER_JUMP_SPEED)
        (setv player.can_jump false)))

  (let [hit_obstacle 0]
    (for [ei env_items]
      (let [p player.position]
        (when 
          (and
            ei.blocking
            (<= ei.rect.x p.x)
            (>= (+ ei.rect.x ei.rect.width) p.x)
            (>= ei.rect.y p.y)
            (<= ei.rect.y (+ p.y (* player.speed delta))))
          (do
            (setv hit_obstacle 1)
            (setv player.speed 0)
            (setv p.y ei.rect.y)))))
    (unless hit_obstacle
      (do
        (setv player.position.y (+ player.position.y (* player.speed delta)))
        (setv player.speed (+ player.speed (* G delta)))
        (setv player.can_jump false))
      (setv player.can_jump true))))

(defn update_camera_center [camera player env_items env_items_length delta width height]
  (setv camera.offset (Vector2 (/ width 2) (/ height 2)))
  (setv camera.target player.position))
