(import pyray *)

(setv screen-width 800)
(setv screen-height 450)

(init-window screen-width screen-height "raylib [core] example - input gestures")

(setv touch-position (Vector2 0))
(setv touch-area (Rectangle 220 10 (- screen-width 230) (- screen-height 20)))

(setv gestures-count 0)
(setv max-gesture-strings 32)
(setv gesture-strings [])

(setv current-gesture GESTURE-NONE)
(setv last-gesture GESTURE-NONE)

(set-target-fps 60)

(while (not (window-should-close))
  (setv last-gesture current-gesture)
  (setv current-gesture (get-gesture-detected))
  (setv touch-position (get-touch-position 0))
  (when (and (check-collision-point-rec touch-position touch-area) (!= current-gesture GESTURE-NONE))
    (when (!= current-gesture last-gesture)
      (cond
        (= current-gesture GESTURE-TAP)
          (.append gesture-strings "GESTURE TAP")
        (= current-gesture GESTURE-DOUBLETAP)
          (.append gesture-strings "GESTURE DOUBLETAP")
        (= current-gesture GESTURE-HOLD)
          (.append gesture-strings "GESTURE HOLD")
        (= current-gesture GESTURE-DRAG)
          (.append gesture-strings "GESTURE DRAG")
        (= current-gesture GESTURE-SWIPE-RIGHT)
          (.append gesture-strings "GESTURE SWIPE RIGHT")
        (= current-gesture GESTURE-SWIPE-LEFT)
          (.append gesture-strings "GESTURE SWIPE LEFT")
        (= current-gesture GESTURE-SWIPE-UP)
          (.append gesture-strings "GESTURE SWIPE UP")
        (= current-gesture GESTURE-SWIPE-DOWN)
          (.append gesture-strings "GESTURE SWIPE DOWN")
        (= current-gesture GESTURE-PINCH-IN)
          (.append gesture-strings "GESTURE PINCH IN")
        (= current-gesture GESTURE-PINCH-OUT)
          (.append gesture-strings "GESTURE PINCH OUT"))
      (+ gestures-count 1)
      (when (>= gestures-count max-gesture-strings)
        (for [i (range max-gesture-strings)]
          (setv (get gesture-strings i) ""))
          (setv gestures-count 0))))
      
    (begin-drawing)
    (clear-background RAYWHITE)
    (draw-rectangle-rec touch-area GRAY)
    (draw-rectangle 225 15 (- screen-width 240) (- screen-height 30) RAYWHITE)
    
    (draw-text "GESTURE TEST AREA" (- screen-width 270) (- screen-height 40) 20 (fade GRAY 0.5))
    
    (for [x (range gestures-count)]
      (if (= (% i 2) 0)
        (draw-rectangle 10 (+ (* 20 i) 30) 200 20 (fade LIGHTGRAY 0.5))
        (draw-rectangle 10 (+ (* 20 i) 30) 200 20 (fade LIGHTGRAY 0.3)))
      (if (< i (- gestures-count 1))
        (draw-text (get gesture-strings i) 35 (+ (* 20 i) 30) 10 DARKGRAY)
        (draw-text (get gesture-strings i) 35 (+ (* 20 i) 30) 10 MAROON)))
    
    (draw-rectangle-lines 10 29 200 (- screen-height 50) GRAY)
    (draw-text "DETECTED GESTURES" 50 15 10 GRAY)
    
    (when (!= current-gesture GESTURE-NONE)
      (draw-circle-v touch-position 30 MAROON))
    (end-drawing))

(close-window)