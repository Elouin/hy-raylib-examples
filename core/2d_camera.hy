(import pyray *)

(setv max-buildings 100)

(setv screen-width 800)
(setv screen-height 450)

(init-window screen-width screen-height "raylib [core] example - 2d camera")

(setv player (Rectangle 400 280 40 40))
(setv buildings [])
(setv build-colors [])

(setv spacing 0)

(for [x (range max-buildings)]
  (let [width (get-random-value 50 200) height (get-random-value 100 800)]
    (.append buildings (Rectangle (+ -6000.0 spacing) (- screen-height 130.0 height) width height)))
  (setv spacing (+ spacing (. (get buildings x) width)))
  (.append build-colors (Color (get-random-value 200 240)
                                (get-random-value 200 240)
                                (get-random-value 200 240)
                                255)))

(setv camera (Camera2D))
(setv (. camera target) (Vector2 (+ (. player x) 20.0) (+ (. player y) 20.0)))
(setv (. camera offset) (Vector2 (/ screen-width 2.0) (/ screen-height 2.0)))
(setv (. camera rotation) 0.0)
(setv (. camera zoom) 1.0)

(set-target-fps 60)

(while (not (window-should-close))
  (cond
    (is-key-down KEY-RIGHT)
      (setv (. player x) (+ (. player x) 2))
    (is-key-down KEY-LEFT)
      (setv (. player x) (- (. player x) 2)))
  
  (setv (. camera target) (Vector2 (+ (. player x) 20.0) (+ (. player y) 20.0)))
  
  (cond
    (is-key-down KEY-A)
      (setv (. camera rotation) (- (. camera rotation) 1))
    (is-key-down KEY-S)
      (setv (. camera rotation) (+ (. camera rotation) 1)))

  (cond 
    (> (. camera rotation) 40.0)
      (setv (. camera rotation) 40.0)
    (< (. camera rotation) -40)
      (setv (. camera rotation) -40))
  
  (setv (. camera zoom) (+ (. camera zoom) (* (get-mouse-wheel-move) 0.05)))
  (cond 
    (> (. camera zoom) 3.0)
      (setv (. camera zoom) 3.0)
    (< (. camera zoom) 0.1)
      (setv (. camera zoom) 0.1))
    
  (when (is-key-pressed KEY-R)
    (do
      (setv (. camera zoom) 1.0)
      (setv (. camera rotation) 0.0)))
      
  (begin-drawing)
  
  (clear-background RAYWHITE)
  
  (begin-mode-2d camera)
  (draw-rectangle -6000 320 13000 8000 DARKGRAY)
  
  (for [i (range max-buildings)]
    (draw-rectangle-rec (get buildings i) (get build-colors i)))
  
  (draw-rectangle-rec player RED)
  
  (draw-line (int (. camera target x)) (* (* screen-height -1) 10) (int (. camera target x)) (* screen-height 10) GREEN)
  (draw-line (* (* screen-width -1) 10) (int (. camera target y)) (* screen_width 10) (int (. camera target y)) GREEN)
  
  (end-mode-2d)
  
  (draw-text "SCREEN AREA" 640 10 20 RED)
  
  (draw-rectangle 0 0 screen-width 5 RED)
  (draw-rectangle 0 5 5 (- screen-height 10) RED)
  (draw-rectangle (- screen-width 5) 5 5 (- screen-height 10) RED)
  (draw-rectangle 0 (- screen-height) screen-width 5 RED)
  
  (draw-rectangle 10 10 250 113 (fade SKYBLUE 0.5))
  (draw-rectangle-lines 10 10 250 113 BLUE)
  
  (draw-text "Free 2d camera controls:" 20 20 10 BLACK)
  (draw-text "- Right/Left to move Offset" 40 40 10 DARKGRAY)
  (draw-text "- Mouse Wheel to Zoom in-out" 40 60 10 DARKGRAY)
  (draw-text "- A / S to Rotate" 40 80 10 DARKGRAY)
  (draw-text "- R to reset Zoom and Rotation" 40 100 10 DARKGRAY)
  
  (end-drawing))

(close-window)