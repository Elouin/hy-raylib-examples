(import pyray *)

(init-window 800 450 "Hello")

(while (not (window-should-close))
  (begin-drawing)
  (clear-background WHITE)
  (draw-text "Hello World" 190 200 20 VIOLET)
  (end-drawing))

(close-window)
